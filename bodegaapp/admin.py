from django.contrib import admin
from .models import Persona, Recurso

admin.site.register(Persona)
admin.site.register(Recurso)
# Register your models here.
