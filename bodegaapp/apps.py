from django.apps import AppConfig


class BodegaappConfig(AppConfig):
    name = 'bodegaapp'
