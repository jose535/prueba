from django import forms
from .models import Persona, Recurso

class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = [
            'identificacion',
            'nombres',
            'apellidos',
        ]
        labels = {
            'identificacion' : 'Identificacion', 
            'nombres' : 'Nombres',
            'apellidos' : 'Apellidos',
        }
        widgets = {
            'identificacion' : forms.NumberInput(attrs={'class':'form-control'}), 
            'nombres' : forms.TextInput(attrs={'class':'form-control'}),
            'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
        }

class RecursoForm(forms.ModelForm):
    class Meta:
        model = Recurso
        fields = [
            'categoria',
            'codigo',
            'nombre',
            'marca',
            'serie',
            'persona',
        ]
        labels = {
            'categoria' : 'Categoria',
            'codigo' : 'Codigo',
            'nombre' : 'Nombre',
            'marca' : 'Marca',
            'serie' : 'Serie',
            'persona' : 'Persona',
        }
        widgets = {
            'categoria' : forms.TextInput(attrs={'class':'form-control'}),
            'codigo' : forms.NumberInput(attrs={'class':'form-control'}),
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'marca' : forms.TextInput(attrs={'class':'form-control'}),
            'serie' : forms.TextInput(attrs={'class':'form-control'}),
            'persona' : forms.Select(attrs={'class':'form-control'}),
        }