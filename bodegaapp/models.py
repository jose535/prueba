from django.db import models

# Create your models here.

class Persona(models.Model):
    identificacion = models.IntegerField(primary_key=True)
    nombres = models.CharField(max_length = 50)
    apellidos = models.CharField(max_length = 50)

    def __str__(self):
        return '%s %s' %(self.nombres, self.apellidos)

class Recurso(models.Model):
    categoria = models.CharField(max_length = 50)
    codigo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length = 50)
    marca = models.CharField(max_length = 50)
    serie = models.CharField(max_length = 50)
    persona = models.ForeignKey(Persona, null = True, blank = True, on_delete = models.CASCADE)

    def __str__(self):
        return self.nombre

