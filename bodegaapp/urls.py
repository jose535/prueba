from django.urls import path
from .views import index, listPersona, listRecursoPersona, personaForm, recursoForm, listRecurso, personaFormEdit, recursoFormEdit

app_name = 'bodega'

urlpatterns = [
    path('', index, name = 'index'),
    path('listPersona',  listPersona, name = 'listPersona'),
    path('listRecurso',  listRecurso, name = 'listRecurso'),
    path('<int:persona_identificacion>/listRecursoPersona', listRecursoPersona, name = 'listRecursoPersona'),
    path('personaNueva', personaForm, name = 'personaNueva'),
    path('recursoNuevo', recursoForm, name = 'recursoNuevo'),
    path('<int:persona_id>/personaEdit', personaFormEdit, name = 'personaEdit'),
    path('<int:recurso_codigo>/recursoEdit', recursoFormEdit, name = 'recursoEdit'),
]
