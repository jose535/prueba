from django.shortcuts import render, redirect
from .models import Persona, Recurso
from .forms import PersonaForm, RecursoForm
# Create your views here.

def index(request):
    return render(request, 'base/base.html')

def listPersona(request):
    personas = Persona.objects.all()
    return render(request, 'bodega/listPersona.html', {'personas' : personas})

def listRecurso(request):
    recursos = Recurso.objects.all()
    return render(request, 'bodega/listRecurso.html', {'recursos' : recursos})

def listRecursoPersona(request, persona_identificacion):
    recursoPersona = Persona.objects.get(identificacion = persona_identificacion)
    return render(request, 'bodega/listRecursoPersona.html', {'recursoPersona' : recursoPersona})

def personaForm(request):
    if request.method == 'POST':
        form = PersonaForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('bodega:listPersona')
    else:
        form = PersonaForm()
    return render(request, 'bodega/crearForm.html', {'form': form})

def recursoForm(request):
    if request.method == 'POST':
        form = RecursoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('bodega:listRecurso')
    else:
        form = RecursoForm()
    return render(request, 'bodega/crearForm.html', {'form': form})

def personaFormEdit(request, persona_id):
    persona = Persona.objects.get(pk = persona_id)
    if request.method == 'GET':
        form = PersonaForm(instance=persona)
    else:
        form = PersonaForm(request.POST, instance=persona)
        if form.is_valid():
            form.save()
        return redirect('bodega:listPersona')
    return render(request, 'bodega/crearForm.html', {'form': form})

def recursoFormEdit(request, recurso_codigo):
    recurso = Recurso.objects.get(codigo = recurso_codigo)
    if request.method == 'GET':
        form = RecursoForm(instance=recurso)
    else:
        form = RecursoForm(request.POST, instance=recurso)
        if form.is_valid():
            form.save()
        return redirect('bodega:listRecurso')
    return render(request, 'bodega/crearForm.html', {'form': form})